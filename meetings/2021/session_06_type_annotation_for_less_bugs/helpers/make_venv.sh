#!/usr/bin/env sh

python3.9 -m venv helpers/venv
source helpers/venv/bin/activate
pip install -r helpers/requirements.txt
