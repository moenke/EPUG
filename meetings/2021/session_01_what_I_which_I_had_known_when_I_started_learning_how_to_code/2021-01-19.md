# EMBL Python User Group - 2021-01-19

## What I wish I had known when I started learning how to code

* Learn how to ask programming questions:
  * http://www.catb.org/~esr/faqs/smart-questions.html

* Find a community of like minded coders
  * EMBL Bio-IT chat #python
  * IRC Freenode #python #pydata #bioinformatics #django
  * Reddit #python
  * Stackoverflow
  * ...

* (ana)conda will mostly save you sanity
  * but pip install is pretty great nowadays
  * If conda is too slow resolving dependencies have a look at https://github.com/mamba-org/mamba

* Virtual environments
  * conda / anaconda
  * venv and virtualenv

* Use an editor that helps you
  * Syntax highlight
  * Code completion
  * Linter - flags errors or potential problems
  * Has debugging abilities
  * [VSCode](https://code.visualstudio.com/) - great for multiple languages, remote interaction (port-forwarding, ...), integrates jupyter notebooks
  * [PyCharm](https://www.jetbrains.com/pycharm/) - also great option, very complete and professional grade IDE (Integrated Desktop Editor) - see [Professional vs Community (free) edition](https://www.jetbrains.com/pycharm/download)
  * [Atom](https://atom.io/) - versatile and modular editor - used as base for other editors (e.g. [Juno](https://junolab.org/) for the Julia programming language)
  * [GitPod](https://www.gitpod.io/).io - Online VSCode-like experience - same environment for everyone needing only a browser to get started - see also [GitPod browser extension](https://www.gitpod.io/docs/browser-extension/)

* Don't worry about performance, readability is more important
  * Be kind to your future self
  * Consider if the more verbose option is more readable than the very concise one
    ```python
    output = {x:y for x in ones for y in twos}
    ```
    vs
    ```python
    output = {}
    for x in ones:
        for y in twos:
           output[x] = y
    ```
  * Use functions, leave classes for later.
  * text is not text -> And why do we need Bytes and Strings
  * Use descriptive names
  * Follow a code style (e.g. PEP8, Google Python Style)
    * A linter will help you (flake8, pylint, ...)
    * Use docstrings - follow sphinx or numpy style - avoid mixing styles
  * Comment/document things!! (in addition to docstrings)
    * Describe **why** not **what** is happening
      * Bad: If we don't have a zero here the code takes 10 times longer to finish
      * Better: This function accepts 0 or 1 and 1 is the slower version
      * Great: Valid values 0, 1: when 1, the code checks if planets are aligned before beginning ritual, this reduces failures but increases execution time.
  * Tests are great, but they might be hard to get right
    Doctests are a great alternative which serves as documentation too e.g:

    ```python
    def divide_and_conquer(values, position):
        """Breaks the list of values in to two at the specified position

        >>> divide_and_conquer([5, 6, 7], 2)
        ([5, 6], [7])
        >>> divide_and_conquer([1, 2], 10)
        ([1, 2], [])
        >>> divide_and_conquer([1, 2], 0)
        ([], [1, 2])
        """
        return values[:position], values[position:]
    ```
    * pytest - great test runner
