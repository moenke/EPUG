# Using GitLab for code reviewing

In this session we used https://git.embl.de and introduced the different tools that it provides to foster collaboration while reviewing and commenting code

The lesson used a sandbox project available at: https://git.embl.de/ralves/sandbox/
