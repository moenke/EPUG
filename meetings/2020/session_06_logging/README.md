# Logging

## Usage

    python intro_logging.py

or

    python -m intro_logging

and with the current configuration you should see the output being saved to a
file `my.log` as well as printed to the console.

Edit `logging.conf` and re-run `intro_logging.py` to see how logged messages
get sent to different destinations depending on the order of loggers, their
level, propagate status and qualname.


## Useful links

* [logging HOWTO](https://docs.python.org/howto/logging.html) - from beginner to advanced
* [logging cookbook](https://docs.python.org/howto/logging-cookbook.html)
* [`logging`](https://docs.python.org/library/logging.html)
* [`logging.config`](https://docs.python.org/library/logging.config.html) - the many ways to configure the logging module
* [`logging.handlers`](https://docs.python.org/library/logging.handlers.html) - different ways to handle logging output
