# -*- coding: utf-8 -*-

import logging
import logging.config


# Using a basic configuration
# logging.basicConfig(format="%(asctime)s %(levelname)-8s %(pathname)s: %(message)s",
#                     level=logging.DEBUG)
#

# or a configuration file to assist and allow customization
logging.config.fileConfig("logging.conf")

# more on configuring logging at:
# https://docs.python.org/howto/logging.html#configuring-logging

# Logs can be sent to different destinations, not just the console output
# You could send to a log server, a file, a different process, via email...
#
# For different handlers see:
# https://docs.python.org/library/logging.handlers.html

# The name passed to getLogger() can be used in logging.conf's qualname
# attribute to target the output of a specific logger
LOG = logging.getLogger(__name__)


def main():
    # Logging uses an implicit severity level
    # See: https://docs.python.org/library/logging.html#logging-levels
    # CRITICAL
    # ERROR
    # WARNING
    # INFO
    # DEBUG
    LOG.critical("HAAA!!! (running around with hands in the air!)")
    LOG.error("OOPS!!")

    # variables can be passed using %s as placeholder
    variable = ("Bob", "is", "here")
    LOG.warning("WARN: %s %s %s!!", *variable)

    LOG.info("This is info")
    LOG.debug("More verbosity")

    other()


def other():
    try:
        1 / 0
    except ZeroDivisionError:
        LOG.exception("We shouldn't get zeros here")

    LOG.critical("But life goes on")


if __name__ == "__main__":
    main()
