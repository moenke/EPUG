# Python and Blender

In this session Kimberly Meechan introduced how she uses Blender (https://blender.org) in her scientific projects.

The files used in this session can be found at: https://git.embl.de/meechan/blender_intro

A slightly more advanced example is included in this repository (see `cube_scrambler_panel.py`)
