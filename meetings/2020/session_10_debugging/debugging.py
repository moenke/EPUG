import os
import glob


def split(path):
    return path.rsplit("/", 1)


def get_path(filename):
    head, tail = split(filename)
    return os.path.abspath(head)


def main(hostname):
    extension = "one/**/*.py"
    files = glob.glob(extension, recursive=True)
    path = get_path(files[0])
    print(">>>>", path, "<<<< on", hostname)


if __name__ == "__main__":
    hostname = os.uname().nodename
    if hostname == "login.cluster.embl.de":
        import debugpy
        print("debugpy loaded and waiting for connection")
        debugpy.listen(12125)
        debugpy.wait_for_client()
        print("debugpy session active")

    main(hostname)