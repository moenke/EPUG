## Python Cinema #1
### [Jake VanderPlas - How to Think about Data Visualization - PyCon 2019](https://www.youtube.com/watch?v=vTingdk_pVM)

Key points mentioned in the video:

- **Anscombe's quartet** (four datasets that have nearly identical simple descriptive statistics, yet have very different distributions and appear very different when graphed (from wikipedia ;))

- **Grammars of visualization**
    * 2D position
    * marks (size, color, shape ...)
    * color scales (depends on data type: quantitative, nominal, ordinal)

- Use a **grammar based charting package**:
    * ggplot2 (for R)
    * plotnine
    * Vega-Lite
    * **Altair**

 
Some of the Python open source libraries we discussed:

- **HoloViews :** Declarative objects for instantly visualizable data, building Bokeh plots from convenient high-level specifications. (http://holoviews.org/)

- **Bokeh :** Interactive plotting in web browsers, running JavaScript but controlled by Python. (https://bokeh.pydata.org/en/latest/)