ETE2 toolkit
------------

- installation?

  pip install ete2

visualization needs qt

- submodules

- ncbi_taxonomy: given a list of taxons, get relationships?

- treeview visualization: what library is used? low-level. How export to PDF/png/svg

- inherit faces? multiple nodes, slightly different look

- use seqmotifface

- imgface: freely available icons of taxons?
- phylotree(alignment)

interactive? svg? webplugin?

set1 ^ set2
