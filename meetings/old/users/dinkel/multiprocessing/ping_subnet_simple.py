#!/usr/bin/env python


import socket
import subprocess

def ping(ip):
    print 'Pinging', ip
    syscall = subprocess.Popen(["ping", "-c", "1", "-W", "1", ip], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    syscall.wait()
    return syscall.returncode

if __name__ == '__main__':
    subnet = '.'.join(socket.gethostbyname(socket.gethostname()).split('.')[:-1]) + '.'

    MAX_HOSTS = 10
    net = [subnet + str(i) for i in range(1,MAX_HOSTS)]
    print net
    for i in net:
        print ping(i)


