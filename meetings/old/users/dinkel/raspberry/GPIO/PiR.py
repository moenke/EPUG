# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import random
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

# <codecell>

red=27
pir=4

GPIO.setup(red, GPIO.OUT)
GPIO.setup(pir, GPIO.IN)

GPIO.output(red, False)

# <codecell>

print 'start'
for i in range(100):
    if GPIO.input(pir):
        print "detected!"
        GPIO.output(red, True)
    time.sleep(1)
    GPIO.output(red, False)
print 'end'

# <codecell>

GPIO.output(red, False)

